<?php

namespace App\Controller;

use App\Engines\HttpEngine;
use App\Entity\User;
use App\Security\TokenAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Contracts\Translation\TranslatorInterface;
use Ramsey\Uuid\Uuid;

class ApiSecurityController extends AbstractController
{

    private $em;
    private $passwordEncoder;
    private $translator;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $this->em = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }

    /**
     * @Route("/auth/login", name="api_login", methods={"GET","POST"})
     */
    public function login(Request $request): Response
    {
        $credentials = (array) $request->request->all();
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);

        if ($user) {
            if ($this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {

                if (null === $user->getApiToken()) {
                    $user->setApiToken(Uuid::uuid4()->toString());
                }

                $lastApiTokenGenerationTime = $user->getApiTokenGenerationTime();
                $diff = $lastApiTokenGenerationTime->diff(new \DateTime());
                $minutes = $diff->days * 24 * 60;
                $minutes += $diff->h * 60;
                $minutes += $diff->i;

                if ($minutes > 14) {
                    $user->setApiToken(Uuid::uuid4()->toString());
                }

                $this->em->flush();
                $data = [
                    'message' => $this->translator->trans('user_authenticated', ['email' => $credentials['email']]),
                    "success" => true,
                    "token" => $user->getApiToken(),
                ];
                return HttpEngine::get()->serialize($data);
            }
        }
        $data = [
            'message' => $this->translator->trans('identity_not_found', ['email' => $credentials['email']]),
            'success' => false
        ];
        return $this->json($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @Route("/auth/register", name="api_register", methods={"GET","POST"})
     */
    public function register(Request $request, GuardAuthenticatorHandler $guardHandler, TokenAuthenticator $authenticator): JsonResponse
    {
        $credentials = json_decode($request->getContent(), true);

        $existingUser = $this->em->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);
        if (null === $existingUser) {
            $user = new User();
            $user->setEmail(in_array('email', $credentials) ? $credentials['email'] : 'no@email.com');
            $user->setIdentity($credentials['email']);
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $credentials['password']
                )
            );
            $this->em->persist($user);
            $this->em->flush();
            // do anything else you need here, like send an email
            $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );

            $data = [
                'message' => $this->translator->trans('user_registered', ['email' => $credentials['email']])
            ];
            return new JsonResponse($data, Response::HTTP_OK);
        } else {
            $data = [
                'message' => $this->translator->trans('user_exists', ['email' => $existingUser->getUsername()])
            ];
            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }
    }
}
