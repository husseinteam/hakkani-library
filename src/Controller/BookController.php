<?php

namespace App\Controller;

use App\Entity\Book;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;

class BookController extends GenericController
{
    public function __construct(EntityManagerInterface $entityManager = null, TranslatorInterface $translator = null)
    {
        if ($entityManager != null && $translator != null) {
            parent::__construct($entityManager, $translator);
        }
    }

    function class()
    {
        return Book::class;
    }

    function resource()
    {
        return 'books';
    }

    function fill(&$wireframe, $json)
    {
        $wireframe->setData($json['data'])
            ->setTitle($json['title'])
            ->setAuthor($json['author']);
    }

    /**
     * @Route("/index", name="books_index")
     */
    public function index(): Response
    {
        $list = $this->repo->findAll();
        return $this->render('book/index.html.twig', [
            'controller_name' => 'BookController',
            'data' => $list,
        ]);
    }

    /**
     * @Route("/download/book", name="book_download", methods={"POST"})
     */
    public function downloadBook(DownloadHandler $downloadHandler, Request $request): Response
    {
        $json = (array) $request->request->all();
        $book = $this->repo->findOneBy(['pdfName' => $json['pdfName']]);
        return $book != null ? $downloadHandler->downloadObject($book, $field = 'pdfFile', $className = null, $forceDownload = false) : $this->serializeData([
            'message' => 'Kitap Bulunamadı',
            'error' => true
        ]);
    }


    /**
    * @Route("/uploads/books/{basename}", name="book_uploads")
    */
    public function downloadUploadedBook($basename, DownloadHandler $downloadHandler): StreamedResponse
    {
        $book = $this->repo->findOneBy(['pdfName' => $basename]);
        return $downloadHandler->downloadObject($book, $field = 'pdfFile', $className = null, $forceDownload = false);
    }
}
