<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class UserController extends GenericController
{
    public function __construct(EntityManagerInterface $entityManager = null, TranslatorInterface $translator = null)
    {
        if ($entityManager != null && $translator != null) {
            parent::__construct($entityManager, $translator);
        }
    }
    function class()
    {
        return User::class;
    }
    function resource()
    {
        return 'users';
    }
    function fill(&$wireframe, $json)
    {
        $wireframe->setEmail($json['email'])
            ->setIdentity($json['identity'])
            ->setApiToken($json['apiToken'])
            ->setPassword($json['password']);
    }

    /**
     * @Route("users/current", name="user_current", methods={"GET"})
     */
    public function current_user(Security $security): Response
    {
        $user = $security->getUser();
        $data = $data = [
            'message' => $this->translator->trans('signedin_user_fetched'),
            "success" => true,
            "user" => $user,
            "token" => $user->getApiToken(),
        ];
        return $this->serializeData($data);
    }
}
