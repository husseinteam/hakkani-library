<?php

namespace App\Admin;

use App\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;

final class CategoryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Content', [
                'class' => 'col-md-12',
                'label' => 'label.admin.category_info',
                'box_class' => 'box box-solid box-primary',
            ])
            ->add('parent', ModelType::class, [
                'class' => Category::class,
                'property' => 'title',
                'label' => 'label.admin.category_parent_title',
                'btn_add' => false,
                'required' => false,
            ])
            ->add('title', TextType::class, [
                'label' => 'label.admin.category_title',
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
        ->addIdentifier('title', TextType::class, [
            'label' => 'label.admin.category_title',
        ])
        ->add('parent', TextType::class, [
            'label' => 'label.admin.category_parent_title',
        ]);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            //->tab('General') // the tab call is optional
            ->with('Categories', [
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid box-primary',
                'label' => 'label.admin.category_info',
            ])
            ->add('title', TextType::class, [
                'label' => 'label.admin.category_title',
            ])
            // ...
            ->end()
            //->end()
        ;
    }

    public function preUpdate($entity): void
    {
        $entity->setUpdatedAt(new \DateTimeImmutable());
    }

    protected $classnameLabel = 'Kitap Kategorisi';
}