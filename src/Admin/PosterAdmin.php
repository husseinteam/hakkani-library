<?php

namespace App\Admin;

use App\Entity\Poster;
use App\Entity\Book;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Show\ShowMapper;
use Vich\UploaderBundle\Form\Type\VichFileType;

final class PosterAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Content', [
                'class' => 'col-md-9', 
                'label' => 'label.admin.poster_info', 
                'box_class' => 'box box-solid box-primary',
            ])
            ->add('posterOriginalName', TextType::class, [
                'label' => 'label.admin.poster_base_name',
                'disabled' => true,
            ])
            ->add('posterFile', VichFileType::class, [
                'label' => 'label.admin.poster_data',
                'required' => $this->isCurrentRoute('create'),
                'allow_delete' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '20480k',
                        'mimeTypes' => [
                            'image/bmp',
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => "Resim değil ya da 20MB'dan büyük",
                    ])
                ],
            ])
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('posterOriginalName', TextType::class, [
                'label' => 'label.admin.poster_base_name',
            ])
            ->add('book', ModelType::class, [
                'class' => Book::class,
                'choice_label' => 'title',
                'label' => 'label.admin.poster_book_title',
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('Content', [
                'class' => 'col-md-9', 
                'label' => 'label.admin.poster_info', 
                'box_class' => 'box box-solid box-primary',
            ])
            ->add('posterOriginalName', TextType::class, [
                'disabled' => true,
                'label' => 'label.admin.poster_base_name',
            ])
            ->end()
            ->with('Meta data', [
                'class' => 'col-md-3',
                'box_class' => 'box box-solid box-success',
                'label' => 'label.admin.poster_metadata'
            ])
            ->add('book', ModelType::class, [
                'class' => Book::class,
                'property' => 'title',
                'label' => 'label.admin.poster_book_title',
            ])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('posterOriginalName')
            ->add('book', null, [], EntityType::class, [
                'class' => Book::class,
                'choice_label' => 'title',
                'label' => 'label.admin.poster_book_title',
            ]);
    }

    public function prePersist($image): void
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image): void
    {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        $image->refreshUpdated();
    }

    protected $classnameLabel = 'Pdf Kapak';
}