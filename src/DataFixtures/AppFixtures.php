<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Finder\Finder;
use App\Entity\User;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\Poster;
use App\Entity\Author;

// php bin/console make:migration
// php bin/console doctrine:migrations:migrate
// php bin/console doctrine:fixtures:load
class AppFixtures extends Fixture
{
    private $encoder;
    private $book_path;
    private $poster_path;

    public function __construct(UserPasswordEncoderInterface $encoder, $book_path, $poster_path)
    {
        $this->encoder = $encoder;
        $this->book_path = $book_path;
        $this->poster_path = $poster_path;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $password = $this->encoder->encodePassword($admin, 'UgrBstn@1957');
        $admin
            ->setIdentity('ugur-bostan')
            ->setEmail("ugurbostan@hakkani.org")
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($password);
        $manager->persist($admin);
        $manager->flush();

        $admin2 = new User();
        $password = $this->encoder->encodePassword($admin2, '293117');
        $admin2
            ->setIdentity('25871077074')
            ->setPassword($password)
            ->setRoles(["ROLE_ADMIN"])
            ->setEmail("hussein.sonmez@outlook.com");
        $manager->persist($admin2);
        $manager->flush();
        
        $user = new User();
        $password = $this->encoder->encodePassword($user, '123456**//');
        $user
            ->setIdentity('restricted_user')
            ->setPassword($password)
            ->setRoles(["ROLE_USER"])
            ->setEmail("destek@natro.com");
        $manager->persist($user);
        $manager->flush();

        $pdf_finder = new Finder();
        $pdf_finder->files()->in($this->book_path)->name('*.pdf');

        $poster_finder = new Finder();
        $poster_finder->files()->in($this->poster_path)->name('*.png');

        $folder = (new Category())
            ->setTitle("Hakkani Kütüphanesi");
        $manager->persist($folder);
        $manager->flush();
        foreach ($pdf_finder as $pdf) {
            $book = new Book();
            list($title, $author) = explode("-", $pdf->getBasename());
            $author = (new Author())->setFullName(explode(".", $author)[0]);
            $manager->persist($author);
            $manager->flush();
            foreach ($poster_finder as $png) {
                if (explode("-", $png->getBasename())[0] === $title) {
                    $poster = new Poster();
                    $poster
                        ->setPosterName($png->getRelativePathname());
                    $manager->persist($poster);
                    $manager->flush();
                    $book
                        ->setPdfName($pdf->getRelativePathname())
                        ->setTitle($title)
                        ->setPoster($poster)
                        ->setAuthor($author)
                        ->setCategory($folder);
                    $manager->persist($book);
                    $manager->flush();
                }
            }
            $manager->flush();
        }
    }
}