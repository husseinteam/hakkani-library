<?php

namespace App\Tests\ControllerTests;

use App\Controller\BookController;
use App\Entity\Book;
use App\Entity\Category;
use Symfony\Component\Finder\Finder;

class BookTest extends GenericControllerTest
{
    protected function wireframe(): object
    {
        $book_path = parent::$container->get('kernel')->getProjectDir() . '/assets/books/';
        $book_finder = new Finder();
        $book_finder->files()->in($book_path)->name('*.pdf');
        $iterator = $book_finder->getIterator();
        $iterator->rewind();
        $book = $iterator->current();
        list($title, $author) = explode('-', $book->getBasename());
        $author = explode(".", $author)[0];
        $folder = (new Category())
            ->setTitle("Meşayıhların Kitapları");
        $entity = (new Book())
            ->setPdfName($book->getBasename())
            ->setTitle($title)
            ->setAuthor($author)
            ->setCategory($folder);
        return $entity;
    }

    protected function editors(): array
    {
        return [
            'author' => function ($wireframe, $prop) {
                return $wireframe->setAuthor($prop);
            }
        ];
    }

    protected function editProps(): array
    {
        return [
            'author' => 'Gavs-ül Azam Abdülkadir Geylani Hazretleri',
        ];
    }

    protected function referenceProps(): array
    {
        return [];
    }
    
    protected function exclusionProps(): array
    {
        return ['data'];
    }
    
    protected function controller()
    {
        return BookController::class;
    }
}
