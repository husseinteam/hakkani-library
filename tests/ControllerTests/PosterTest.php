<?php

namespace App\Tests\ControllerTests;

use App\Controller\PosterController;
use App\Entity\Poster;
use App\Repository\BookRepository;
use Symfony\Component\Finder\Finder;

class PosterTest extends GenericControllerTest
{
    private $book;
    protected function wireframe(): object
    {
        $poster_path = parent::$container->get('kernel')->getProjectDir() . '/assets/posters/';
        $poster_finder = new Finder();
        $poster_finder->files()->in($poster_path)->name('*.png');
        $iterator = $poster_finder->getIterator();
        $iterator->rewind();
        $poster = $iterator->current();
        list($title, ) = explode('-', $poster->getBasename());
        $this->book = parent::$container->get(BookRepository::class)
            ->findOneBy(['title' => $title]);
        $entity = (new Poster())
            ->setPosterName($poster->getPathname())
            ->setPosterName($poster->getBasename())
            ->setBook($this->book);
        return $entity;
    }

    protected function editors(): array
    {
        return [
            'posterName' => function ($wireframe, $prop) {
                return $wireframe->setBaseName($prop);
            }
        ];
    }

    protected function editProps(): array
    {
        return [
            'posterName' => 'İlahi armağan-Şeyh Abdülkadir Geylani.png',
        ];
    }

    protected function referenceProps(): array
    {
        return ['book_id' => $this->book->getId()];
    }

    protected function exclusionProps(): array
    {
        return ['data'];
    }
    
    protected function controller()
    {
        return PosterController::class;
    }
}
