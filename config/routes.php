<?php

// config/routes.php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use App\Controller\PosterController;
use App\Controller\UserController;
use App\Controller\BookController;
use App\Controller\CategoryController;
use App\Controller\AuthorController;

// php bin/console debug:router
return function (RoutingConfigurator $routes) {
    $controllerIds = [CategoryController::class, BookController::class, PosterController::class, AuthorController::class];
    foreach ($controllerIds as $controllerId) {
        $resource = (new $controllerId())->resource();
        $routes->add("{$resource}_list", "/$resource/list")
            ->controller([$controllerId, 'list'])
            ->methods(['GET']);
        $routes->add("{$resource}_single", "/$resource/single/{id}")
            ->controller([$controllerId, 'single'])
            ->methods(['GET'])
            ->requirements(['id' => '\d+']);
        $routes->add("{$resource}_insert", "/$resource/insert")
            ->controller([$controllerId, 'insert'])
            ->methods(['POST']);
        $routes->add("{$resource}_edit", "/$resource/{id}/edit")
            ->controller([$controllerId, 'edit'])
            ->methods(['POST'])
            ->requirements(['id' => '\d+']);
        $routes->add("{$resource}_delete", "/$resource/{id}/delete")
            ->controller([$controllerId, 'delete'])
            ->methods(['POST'])
            ->requirements(['id' => '\d+']);
    }
};
