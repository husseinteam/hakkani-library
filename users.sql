/*
-- Query: SELECT * FROM reader_db.user
LIMIT 0, 1000

-- Date: 2021-01-20 21:25
*/
INSERT INTO `user` (`id`,`email`,`identity`,`api_token`,`roles`,`password`,`api_token_generation_time`,`generated_at`,`updated_at`) VALUES (1,'ugurbostan@hakkani.org','ugur-bostan',NULL,'[\"ROLE_ADMIN\"]','$2y$13$cWkFJG71NKoMOSuXieUQs.wHzwJcUWn4v46.QIq3yOvAPGpf3gP0K',NULL,'2021-01-20 17:39:37',NULL);
INSERT INTO `user` (`id`,`email`,`identity`,`api_token`,`roles`,`password`,`api_token_generation_time`,`generated_at`,`updated_at`) VALUES (2,'hussein.sonmez@outlook.com','25871077074',NULL,'[]','$2y$13$3R.ZjpgZiXIm9kucGWvYbur3SzNd8NxPSx3rAGv2/91t1eSPoqk1.',NULL,'2021-01-20 17:39:37',NULL);
